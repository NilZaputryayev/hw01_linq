﻿using DataLoader.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DataLoader
{
    public static class Loader
    {
        private static readonly HttpClient client = new HttpClient();

        readonly static string BaseUrl = "https://bsa21.azurewebsites.net/api/";

        readonly static string ProjectsEndpoint = "Projects";
        readonly static string TasksEndpoint = "Tasks";
        readonly static string TeamsEndpoint = "Teams";
        readonly static string UsersEndpoint = "Users";
        private static JsonSerializerSettings jsonSettings;

        public static void ConfigureClient()
        {
            client.BaseAddress = new Uri(BaseUrl);
            client.DefaultRequestHeaders.Accept.ParseAdd("application/json");

            jsonSettings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        public static async Task<List<T>> Get<T>()
        {
            List<T> data = new List<T>();

            string endpoint = GetEndpoint(typeof(T).ToString());

            try
            {
                var response = await client.GetAsync(endpoint);

                if (response.IsSuccessStatusCode)


                    data = JsonConvert.DeserializeObject<List<T>>(await response.Content.ReadAsStringAsync(), jsonSettings);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return data;


        }

        private static string GetEndpoint(string type)
        {
            // remove namespace from type name
            switch (type.Split('.').Last())
            {
                case "User": return UsersEndpoint;
                case "Team": return TeamsEndpoint;
                case "Project": return ProjectsEndpoint;
                case "Task": return TasksEndpoint;
                default: return string.Empty;
            }
        }
    }
}
