﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLoader.Models
{

    public class Project
    {
        public int id { get; set; }
        public int authorId { get; set; }
        public int teamId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime deadline { get; set; }
        public IEnumerable<Task> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }

        public override string ToString()
        {
            return $"id: {id}, authorId:{authorId}, teamId:{teamId}, name:{name}, description: {description}, created:{createdAt.ToShortDateString()}, deadline:{deadline.ToShortDateString()} ";
        }

    }



}
