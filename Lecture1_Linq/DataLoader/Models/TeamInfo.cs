﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLoader.Models
{
    public class TeamInfo
    {
        public int id { get; set; }
        public string name { get; set; }
        public IEnumerable<User> users { get; set; }
        
    }
}
