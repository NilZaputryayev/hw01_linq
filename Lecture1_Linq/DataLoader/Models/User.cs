﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLoader.Models
{
    public class User
    {
        public int id { get; set; }
        public int teamId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public DateTime registeredAt { get; set; }
        public DateTime birthDay { get; set; }

        public override string ToString()
        {
            return $"id: {id}, teamId:{teamId}, firstName:{firstName}, lastName:{lastName}, email: {email}, registeredAt:{registeredAt.ToShortDateString()}, birthDay:{birthDay.ToShortDateString()} ";
        }
    }


}
