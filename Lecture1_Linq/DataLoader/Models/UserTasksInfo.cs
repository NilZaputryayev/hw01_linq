﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLoader.Models
{
    public class UserTasksInfo
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }   
        public int CountCancelOrUnfinishedTasks { get; set; }           
        public Task LongestTask { get; set; }

        public override string ToString()
        {
            return $"User:\n\r {User}\n\rProject:\n\r{LastProject}\n\rLastProjectTasksCount:{LastProjectTasksCount}\n\rCountCancelOrUnfinishedTasks:{CountCancelOrUnfinishedTasks}\n\rLatTask: {LongestTask}";
        }

        public void Print()
        {
            Console.WriteLine($"User:\n\r{User.ToString()}");
            Console.WriteLine($"LastProject:\n\r{LastProject.ToString()}");
            Console.WriteLine($"LastProjectTasksCount: {LastProjectTasksCount}");
            Console.WriteLine($"CountCancelOrUnfinishedTasks: {CountCancelOrUnfinishedTasks}");
            Console.WriteLine($"LongestTask:\n\r{LongestTask.ToString()}");
        }

    }
}
