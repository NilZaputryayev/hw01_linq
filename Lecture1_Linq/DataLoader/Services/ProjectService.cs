﻿using DataLoader.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DataLoader
{
    public class ProjectService : BaseService<Project> 
    {

        public ProjectService() : base() {
            Endpoint = "Projects";
        }

        public override Task<List<Project>> Get()
        {
            return base.Get();
        }
    }
}
