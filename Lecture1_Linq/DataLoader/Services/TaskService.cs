﻿using DataLoader.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace DataLoader
{
    public class TaskService : BaseService<Task> 
    {

        public TaskService() : base() {
            Endpoint = "Tasks";
        }

        public override System.Threading.Tasks.Task<List<Task>> Get()
        {
            return base.Get();
        }
    }
}
