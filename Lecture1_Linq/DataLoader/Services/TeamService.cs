﻿using DataLoader.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DataLoader
{
    public class TeamService : BaseService<Team> 
    {

        public TeamService() : base() {
            Endpoint = "Teams";
        }

        public override Task<List<Team>> Get()
        {
            return base.Get();
        }
    }
}
