﻿using DataLoader.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DataLoader
{
    public class UserService : BaseService<User> 
    {

        public UserService() : base() {
            Endpoint = "Users";
        }

        public override Task<List<User>> Get()
        {
            return base.Get();
        }
    }
}
