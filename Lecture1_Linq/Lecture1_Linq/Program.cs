﻿using DataLoader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using DataLoader;

namespace Lecture1_Linq
{
    class Program
    {
        static IEnumerable<Project> ProjectData;
        static IEnumerable<Team> TeamData;

        static readonly UserService userService = new UserService();
        static readonly ProjectService projectService = new ProjectService();
        static readonly TeamService teamService = new TeamService();
        static readonly TaskService taskService = new TaskService();

        static async System.Threading.Tasks.Task Main(string[] args)
        {
            await GetData();

            bool showMenu = true;
            while (showMenu)
            {
                showMenu = MainMenu();
                Console.WriteLine("Press any key");
                Console.ReadKey();
            }

        }

        private static async System.Threading.Tasks.Task GetData()
        {
            var users = await userService.Get();
            var projects = await projectService.Get();
            var teams = await teamService.Get();
            var tasks = await taskService.Get();



            IEnumerable<Task> taskPerformers = from t in tasks
                                               join u in users on t.performerId equals u.id into tp
                                               from task_performer in tp
                                               select new Task
                                               {
                                                   id = t.id,
                                                   createdAt = t.createdAt,
                                                   description = t.description,
                                                   finishedAt = t.finishedAt,
                                                   state = t.state,
                                                   name = t.name,
                                                   performerId = t.performerId,
                                                   projectId = t.projectId,
                                                   Performer = task_performer
                                               };

            teams = (from t in teams
                     join u in users on t.id equals u.teamId into team_users
                     select new Team
                     {
                         id = t.id,
                         createdAt = t.createdAt,
                         name = t.name,
                         users = team_users
                     }).ToList();



            ProjectData = from p in projects
                          join a in users on p.authorId equals a.id into pa
                          from project_author in pa
                          join m in teams on p.teamId equals m.id into pt
                          from project_team in pt
                          join t in taskPerformers on p.id equals t.projectId into project_tasks
                          select new Project
                          {
                              id = p.id,
                              authorId = p.authorId,
                              teamId = p.teamId,
                              name = p.name,
                              description = p.description,
                              createdAt = p.createdAt,
                              deadline = p.deadline,
                              Tasks = project_tasks,
                              Author = project_author,
                              Team = project_team
                          };

            TeamData = from t in teams
                       join u in users on t.id equals u.teamId into team_members
                       select new Team
                       {
                           createdAt = t.createdAt,
                           name = t.name,
                           id = t.id,
                           users = team_members
                       };
        }

        private static bool MainMenu()
        {

            Console.Clear();

            Console.WriteLine("Select an option:");
            Console.WriteLine("1. Get count of tasks from user (id)");
            Console.WriteLine("2. Get list of tasks where name < 45 (id)");
            Console.WriteLine("3. List of finished in 2021 year tasks (id)");
            Console.WriteLine("4. List (id, name, users) from teams where users age > 10");
            Console.WriteLine("5. List of users sorted by first_name & task sorted by name desc");
            Console.WriteLine("6. Get UserInfo  (id)");
            Console.WriteLine("7. Get Project info (id)");
            Console.WriteLine("0. Exit");
            Console.Write("\r\nSelect an option: ");

            switch (Console.ReadLine())
            {
                case "1":

                    Task1();
                    return true;
                case "2":
                    Task2();
                    return true;
                case "3":
                    Task3();
                    return true;
                case "4":
                    Task4();
                    return true;
                case "5":
                    Task5();
                    return true;
                case "6":
                    Task6();
                    return true;
                case "7":
                    Task7();
                    return true;
                case "0":
                    return false;
                default:
                    return true;
            }

        }

        private static void Task1()
        {
            if (Int32.TryParse(GetStringFromConsole("userID"), out int id))
                Print(GetUserTaskCountDictionaryByID(id));
        }
        private static void Task2()
        {
            if (Int32.TryParse(GetStringFromConsole("userID"), out int id))
                Print(ListTasksWith45NameByID(id));
        }
        private static void Task3()
        {
            if (Int32.TryParse(GetStringFromConsole("userID"), out int id))
                Print(TaskListFinished2021ByID(id));
        }
        private static void Task4()
        {
            Print(GetTeamsOlder10SortedAndGrouped_FromTeamDataStructure());
        }

        private static void Print(List<TeamInfo> result)
        {
            if (result.Count == 0)
            {
                Console.WriteLine($"No data");
                return;
            }


            foreach (var item in result)
            {
                Console.WriteLine($"Team id: {item.id.ToString()}");
                Console.WriteLine($"Team name: {item.name.ToString()}");

                Console.WriteLine($"Team users count {item.users.Count()}");

                foreach (var user in item.users)
                {
                    Console.WriteLine(user.ToString());
                }

            }
        }

        private static void Task5()
        {
            Print(GetUserListSortedWithTasks());
        }

        private static void Task6()
        {
            if (Int32.TryParse(GetStringFromConsole("userID"), out int id))
                GetUserTasksInfo(id)?.Print();
        }

        private static void Task7()
        {
            if (Int32.TryParse(GetStringFromConsole("projectID"), out int id))
                GetProjectInfo(id)?.Print();
        }



        private static void Print(Dictionary<string, List<Task>> result)
        {
            if (result.Count == 0)
            {
                Console.WriteLine($"No data");
                return;
            }

            Console.WriteLine($"Tasks count {result.Values.Count()}");

            foreach (var item in result)
            {
                Console.WriteLine($"User {item.Key.ToString()}");
                foreach (var task in item.Value)
                {
                    Console.WriteLine($"TaskNameLen:{task.name.Length} Tasks {task.ToString()}");
                }

            }
        }
        private static void Print(List<Tuple<int, string>> result)
        {
            if (result.Count == 0)
            {
                Console.WriteLine($"No data");
                return;
            }

            Console.WriteLine($"Tasks count {result.Count()}");

            foreach (var item in result)
            {
                Console.WriteLine($"Project {item.ToString()}");

            }
        }
        private static void Print(List<Task> result)
        {
            if (result.Count == 0)
            {
                Console.WriteLine($"No data");
                return;
            }

            Console.WriteLine($"Tasks count {result.Count()}");

            foreach (var item in result)
            {
                Console.WriteLine($"Project {item.ToString()}");

            }


        }
        private static void Print(Dictionary<Project, int> result)
        {

            if (result.Count == 0)
            {
                Console.WriteLine($"No data");
                return;
            }

            Console.WriteLine($"Tasks count {result.Values.Sum()}");

            foreach (var item in result)
            {
                Console.WriteLine($"Project {item.Key.ToString()}");
                Console.WriteLine($"Count tasks {item.Value.ToString()}");

            }
        }




        private static string GetStringFromConsole(string message)
        {
            string result;
            do
            {
                Console.WriteLine($"Enter {message}:");
                result = Console.ReadLine();

            } while (string.IsNullOrEmpty(result));

            return result;
        }
        private static Dictionary<Project, int> GetUserTaskCountDictionaryByID(int id)
        {
            return ProjectData
                     .ToDictionary(key => key, value => value.Tasks.Where(x => x.performerId == id).Count())
                     .Where(x => x.Value > 0)               // Need to filter by empty projects but give us KeyVAluePair so we need to convert it back
                     .ToDictionary(x => x.Key, y => y.Value);  // Convert from KeyValuePair to Dictionary
        }
        private static List<Task> ListTasksWith45NameByID(int id)
        {
            return ProjectData
                    .SelectMany(x => x.Tasks)
                    .Where(x => x.name.Length < 45 &&
                                x.performerId == id
                          )
                    .ToList();
        }
        private static List<Tuple<int, string>> TaskListFinished2021ByID(int id)
        {
            return ProjectData
                    .SelectMany(x => x.Tasks)
                    .Where(x => x.finishedAt > new DateTime(2021, 01, 01) &&
                                x.performerId == id)
                    .Select(x => new Tuple<int, string>(x.id, x.name))
                    .ToList();
        }
        private static Dictionary<string, List<Task>> GetUserListSortedWithTasks()
        {
            return ProjectData
                    .SelectMany(x => x.Tasks)
                    .Select(x => new
                    {
                        first_name = x.Performer.firstName,
                        task = x
                    })
                    .OrderBy(x => x.first_name)
                    .ThenByDescending(x => x.task.name.Length)
                    .GroupBy(x => x.first_name, y => y.task)
                    .ToDictionary(x => x.Key, y => y.ToList());
        }

        /// <summary>
        /// Get data from TeamData structure (joined teams and users )
        /// </summary>
        /// <returns></returns>
        private static List<TeamInfo> GetTeamsOlder10SortedAndGrouped_FromTeamDataStructure()
        {
            return TeamData
                    .Where(x => x.users.All(u => DateTime.Now.Year - 10 > u.birthDay.Year))
                    .Select(x => new TeamInfo
                    {
                        id = x.id,
                        name = x.name,
                        users = x.users.OrderByDescending(x => x.registeredAt)
                    })
                    //.GroupBy(x => x.name)
                    .ToList();

        }



        /// <summary>
        /// If we get data from this structure we can't get teams and users that have no projects (
        /// </summary>
        /// <returns></returns>
        private static Dictionary<int, TeamInfo> GetTeamsOlder10SortedAndGrouped_FromProjectDataStructure()
        {
            return ProjectData
                        .SelectMany(p => p.Team.users, (p, u) => new { Project = p, Users = u })
                        .GroupBy(x => x.Users.teamId)
                        .Where(x => x.All(u => DateTime.Now.Year - 10 > u.Users.birthDay.Year))
                        .Select(t =>
                         new
                         {
                             id = t.Key,
                             name = t.FirstOrDefault(x => x.Project.Team.id == t.Key).Project.Team.name,
                             users = t.Where(u => u.Users.teamId == t.Key)
                                           .OrderByDescending(x => x.Users.registeredAt)
                                           .Select(u => new User()
                                           {
                                               id = u.Users.id,
                                               birthDay = u.Users.birthDay,
                                               email = u.Users.email,
                                               firstName = u.Users.firstName,
                                               lastName = u.Users.lastName,
                                               registeredAt = u.Users.registeredAt,
                                               teamId = u.Users.teamId
                                           })

                         })
                        .ToDictionary(x => x.id, y => new TeamInfo() { id = y.id, name = y.name, users = y.users });
        }

        private static UserTasksInfo GetUserTasksInfo(int id)
        {
            return ProjectData
                    .SelectMany(p => p.Tasks, (p, t) => new { Project = p, Task = t })
                    .GroupBy(x => x.Task.Performer)
                    .Where(x => x.Key.id == id)
                    .Select(x => new UserTasksInfo()
                    {
                        User = x.Key,

                        LastProject = x.OrderByDescending(x => x.Project.createdAt)
                                        .FirstOrDefault().Project,

                        LastProjectTasksCount = x.OrderByDescending(x => x.Project.createdAt)
                                                    .Select(x => x.Project.Tasks.Count())
                                                    .FirstOrDefault(),

                        CountCancelOrUnfinishedTasks = x.Where(x => x.Task.finishedAt == DateTime.MinValue)
                                                        .Count(),

                        LongestTask = x.Where(x => x.Task.finishedAt > DateTime.MinValue)
                                        .Select(x => new { task = x, taskLen = x.Task.finishedAt - x.Task.createdAt })
                                        .OrderByDescending(x => x.taskLen)
                                        .Select(x => x.task.Task)
                                        .FirstOrDefault()
                    }).FirstOrDefault();
        }


        /// <summary>
        /// Optimal but one method is wrong (
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private static ProjectInfo GetProjectInfo_problem(int id)
        {
            return ProjectData
                    .SelectMany(p => p.Tasks, (p, t) => new { Project = p, Task = t })
                    .Where(x => x.Project.id == id)
                    .Select(x => new ProjectInfo()
                    {
                        Project = x.Project,

                        LongestTaskByDescription = x.Project.Tasks.Select(x => new { task = x, descriptionLen = x.description.Length })
                                                    .OrderByDescending(x => x.descriptionLen)
                                                    .Select(x => x.task)
                                                    .FirstOrDefault(),

                        ShortestTaskByName = x.Project.Tasks.Select(x => new { task = x, nameLen = x.name.Length })
                                              .OrderBy(x => x.nameLen)
                                              .Select(x => x.task)
                                              .FirstOrDefault(),


                        //CountUsersByCondition = x.Project.Team.users.Count(x => x.description.Length > 20 || x.Count() < 3)
                        //                            .Select(x => x.Project.Team.users.Count())
                        //                            .FirstOrDefault()

                    }).FirstOrDefault();
        }


        /// <summary>
        /// Not optimal but work
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private static ProjectInfo GetProjectInfo(int id)
        {
            return ProjectData
                    .SelectMany(p => p.Tasks, (p, t) => new { Project = p, Task = t })
                    .GroupBy(x => x.Project)
                    .Select(x => new ProjectInfo()
                    {
                        Project = x.Key,

                        LongestTaskByDescription = x.Select(x => new { task = x, descriptionLen = x.Task.description.Length })
                                                    .OrderByDescending(x => x.descriptionLen)
                                                    .Select(x => x.task.Task)
                                                    .FirstOrDefault(),

                        ShortestTaskByName = x.Select(x => new { task = x, nameLen = x.Task.name.Length })
                                              .OrderBy(x => x.nameLen)
                                              .Select(x => x.task.Task)
                                              .FirstOrDefault(),


                        CountUsersByCondition = x.Where(x => x.Project.description.Length > 20 || x.Project.Tasks.Count() < 3)
                                                    .Select(x => x.Project.Team.users.Count())
                                                    .FirstOrDefault()

                    }).FirstOrDefault(x => x.Project.id == id);
        }


    }
}
